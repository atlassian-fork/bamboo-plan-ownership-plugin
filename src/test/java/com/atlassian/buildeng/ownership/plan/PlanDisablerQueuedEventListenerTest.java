/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.plan;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.plan.*;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.events.BuildQueuedEvent;
import com.atlassian.buildeng.ownership.PlanDisablerQueuedEventListener;
import com.atlassian.buildeng.ownership.PlanOwnershipValidatorService;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import com.google.common.collect.Maps;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class PlanDisablerQueuedEventListenerTest extends TestCase {

    @InjectMocks
    @Spy
    private PlanDisablerQueuedEventListener planDisablerQueuedEventListener;

    @Mock
    private PlanOwnershipConfigService planOwnershipConfigService;

    @Mock
    private PlanOwnershipValidatorService planOwnershipValidatorService;

    @Mock
    private PlanExecutionManager planExecutionManager;

    @Mock
    private AdministrationConfigurationAccessor administrationConfigurationAccessor;

    @Mock
    private PlanManager planManager;

    @Mock
    private BambooUserManager bambooUserManager;

    @Mock
    private BuildLoggerManager buildLoggerManager;

    @Mock
    private BuildLogger buildLogger;

    @Mock
    private BuildContext buildContext;

    @Mock
    private BuildDefinition buildDefinition;

    @Mock
    private BuildContext parentBuildContext;

    @Mock
    private BuildQueuedEvent event;

    @Mock
    private Plan parentPlan;

    @Mock
    private AdministrationConfiguration administrationConfiguration;

    @Mock
    private BambooUser bambooUser;

    @Before
    public void setUp() {
        when(buildLoggerManager.getLogger(Matchers.any(PlanResultKey.class))).thenReturn(buildLogger);

        when(event.getContext()).thenReturn(buildContext);
        when(event.getPlanKey()).thenReturn(PlanKeys.getPlanKey("PLAN-KEY"));
        when(buildContext.getParentBuildContext()).thenReturn(parentBuildContext);
        when(buildContext.getTypedPlanKey()).thenReturn(PlanKeys.getPlanKey("PLAN-KEY"));
        when(parentBuildContext.getTypedPlanKey()).thenReturn(PlanKeys.getPlanKey("PARENTPLAN-KEY"));

        Map<String, String> customConfiguration = Maps.newHashMap();
        customConfiguration.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, "123fake");

        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        when(parentBuildContext.getBuildDefinition()).thenReturn(buildDefinition);

        when(administrationConfigurationAccessor.getAdministrationConfiguration()).thenReturn(administrationConfiguration);
        when(administrationConfiguration.getBaseUrl()).thenReturn("localhost");

        when(bambooUserManager.getBambooUser(Matchers.any(String.class))).thenReturn(bambooUser);

        when(planOwnershipConfigService.isEnforcementEnabled()).thenReturn(true);
        when(planOwnershipConfigService.isPlanDisablementEnabled()).thenReturn(false);

        when(planManager.getPlanByKey(Matchers.any(PlanKey.class))).thenReturn(parentPlan);
    }

    @Test
    public void testDoNotStopPlanIfOwnerIsOk() throws Exception {
        when(planOwnershipValidatorService.isExistingOwnerValid(Matchers.anyString())).thenReturn(true);
        planDisablerQueuedEventListener.onEvent(event);
        verify(parentPlan, times(0)).setSuspendedFromBuilding(eq(true));
        verify(planExecutionManager, times(0)).stopPlan(isA(PlanKey.class), eq(true), isA(String.class));
    }

    @Test
    public void testStopPlanIfInvalidOwner() throws Exception {
        when(planOwnershipValidatorService.isExistingOwnerValid(Matchers.anyString())).thenReturn(false);
        when(buildDefinition.getCustomConfiguration()).thenReturn(Maps.newHashMap());
        planDisablerQueuedEventListener.onEvent(event);
        verify(parentPlan, times(0)).setSuspendedFromBuilding(eq(true));
        verify(planExecutionManager, times(1)).stopPlan(isA(PlanKey.class), eq(true), isA(String.class));
    }

    @Test
    public void testDisablePlanIfInvalidOwner() throws Exception {
        when(buildDefinition.getCustomConfiguration()).thenReturn(Maps.newHashMap());
        when(planOwnershipConfigService.isPlanDisablementEnabled()).thenReturn(true);
        planDisablerQueuedEventListener.onEvent(event);
        verify(parentPlan, times(1)).setSuspendedFromBuilding(eq(true));
        verify(planExecutionManager, times(1)).stopPlan(isA(PlanKey.class), eq(true), isA(String.class));
    }
}
