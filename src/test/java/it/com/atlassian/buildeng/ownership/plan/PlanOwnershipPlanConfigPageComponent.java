/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.com.atlassian.buildeng.ownership.plan;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.AbstractBambooPage;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.testutils.specs.TestPlanSpecsHelper;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class PlanOwnershipPlanConfigPageComponent extends AbstractBambooPage {
    private final Plan plan;

    @ElementBy(id = "updateChainMiscellaneous_custom_planownership_bamboo_plugin_plan_config_ownerOfBuild")
    private TextElement owner;

    @ElementBy(id = "updateChainMiscellaneous_save")
    private PageElement saveButton;

    public PlanOwnershipPlanConfigPageComponent(Plan plan) {
        this.plan = plan;
    }

    public String getUrl() {
        return "chain/admin/config/editChainMiscellaneous.action?buildKey=" + TestPlanSpecsHelper.getPlanKey(plan);
    }

    public PlanOwnershipPlanConfigPageComponent setOwner(String owner) {
        this.owner.setText(owner);
        return this;
    }

    public String getOwner() {
        return owner.getValue();
    }

    public void save() {
        saveButton.click();
    }

    public PageElement indicator() {
        return owner;
    }
}
