/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.event.ChainCreatedEvent;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.security.acegi.acls.HibernateMutableAclService;
import com.atlassian.bamboo.security.acegi.acls.HibernateObjectIdentityImpl;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.events.BuildQueuedEvent;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import com.atlassian.buildeng.ownership.plan.PlanOwnershipPlanConfigurationPlugin;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import java.util.Map;
import org.acegisecurity.acls.Acl;
import org.acegisecurity.acls.sid.PrincipalSid;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class PlanDisablerQueuedEventListener {
    private static final Logger log = LoggerFactory.getLogger(PlanDisablerQueuedEventListener.class);


    private final PlanOwnershipConfigService planOwnershipConfigService;

    private final PlanOwnershipValidatorService planOwnershipValidatorService;

    @ComponentImport
    @Autowired
    private final AdministrationConfigurationAccessor administrationConfigurationAccessor;

    @ComponentImport
    @Autowired
    private final PlanExecutionManager planExecutionManager;

    @ComponentImport
    @Autowired
    private final PlanManager planManager;

    @ComponentImport
    @Autowired
    private final BuildLoggerManager buildLoggerManager;

    @ComponentImport
    @Autowired
    private final HibernateMutableAclService aclService;

    @ComponentImport
    @Autowired
    private final BuildDefinitionManager buildDefinitionManager;

    private static final Logger logger = LoggerFactory.getLogger(PlanDisablerQueuedEventListener.class);

    public PlanDisablerQueuedEventListener(@NotNull PlanOwnershipConfigService planOwnershipConfigService,
            @NotNull PlanExecutionManager planExecutionManager,
            @NotNull PlanManager planManager,
            @NotNull BuildLoggerManager buildLoggerManager,
            @NotNull AdministrationConfigurationAccessor administrationConfigurationAccessor,
            @NotNull PlanOwnershipValidatorService planOwnershipValidatorService,
            @NotNull HibernateMutableAclService aclService,
            @NotNull BuildDefinitionManager buildDefinitionManager) {
        this.planOwnershipConfigService = planOwnershipConfigService;
        this.planExecutionManager = planExecutionManager;
        this.planManager = planManager;
        this.buildLoggerManager = buildLoggerManager;
        this.administrationConfigurationAccessor = administrationConfigurationAccessor;
        this.planOwnershipValidatorService = planOwnershipValidatorService;
        this.aclService = aclService;
        this.buildDefinitionManager = buildDefinitionManager;
    }

    /**
     * When a plan is created by a user that can be owner, preset the value.
     * This typically happens when plans are manually created.
     * @param event fired when creating new plan
     */
    @EventListener
    public void onEvent(ChainCreatedEvent event) {
        Plan plan = planManager.getPlanByKey(event.getPlanKey(), Plan.class);
        if (plan != null && !plan.hasMaster()) {
            Acl acl = aclService.readAclById(new HibernateObjectIdentityImpl(plan));
            if (acl != null && acl.getOwner() instanceof PrincipalSid) {
                PrincipalSid ps = (PrincipalSid)acl.getOwner();
                String owner = ps.getPrincipal();
                if (planOwnershipValidatorService.isSuggestedOwnerValid(owner)) {
                    BuildDefinition buildDefinition = plan.getBuildDefinition();
                    Map<String, String> cd = buildDefinition.getCustomConfiguration();
                    cd.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, owner);
                    buildDefinition.setCustomConfiguration(cd);
                    buildDefinitionManager.savePlanAndDefinition(plan, buildDefinition);
                }
            }
        }
    }

    /**
     * When build is queued.
     */
    @EventListener
    public void onEvent(BuildQueuedEvent buildQueuedEvent) throws Exception {
        BuildContext parentBuildContext = buildQueuedEvent.getContext()
                .getParentBuildContext();
        String planKey = parentBuildContext.getTypedPlanKey().getKey();
        if (planOwnershipConfigService.isEnforcementEnabled()
                && !planOwnershipValidatorService.isExistingOwnerValid(planKey)) {
            PlanKey parentPlanKey = parentBuildContext.getTypedPlanKey();
            PlanResultKey planResultKey = buildQueuedEvent.getPlanResultKey();
            Plan parentPlan = planManager.getPlanByKey(parentPlanKey);
            String baseUrl = this.administrationConfigurationAccessor.getAdministrationConfiguration().getBaseUrl();
            String reason = planOwnershipValidatorService.getReasonForExistingOwner(planKey);
            BuildLogger buildLogger = buildLoggerManager.getLogger(planResultKey);
            buildLogger.addErrorLogEntry("This build has been stopped because: ");
            buildLogger.addErrorLogEntry("\t" + reason);
            buildLogger.addErrorLogEntry("Please correct this via the Other configuration tab of your plan: "
                                      + baseUrl + "/chain/admin/config/editChainMiscellaneous.action?buildKey="
                                      + parentPlanKey.getKey()
                                      + "\nSee https://bitbucket.org/atlassian/bamboo-plan-ownership-plugin/src/master/README.md for details.");
            String logMessage = "Plan Ownership Plugin stopped build: " + planResultKey;
            if (planOwnershipConfigService.isPlanDisablementEnabled()) {
                parentPlan.setSuspendedFromBuilding(true);
                logger.info(logMessage + " and disabled: " + parentPlanKey.getKey());
            } else {
                logger.info(logMessage);
            }
            planManager.savePlan(parentPlan);
            planExecutionManager.stopPlan(buildQueuedEvent.getContext().getTypedPlanKey(), true,
                    "plan stopped by the Plan Ownership Plugin");
        }
    }
}
