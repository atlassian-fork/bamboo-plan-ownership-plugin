<!-- /*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ -->

<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="bamboo-plan-ownership"/>

    <!-- add our web resources -->
    <web-resource key="bamboo-plan-ownership-resources" name="bamboo-plan-ownership Web Resources">
        <dependency>com.atlassian.plugins.jquery:jquery</dependency>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="jquery-autocomplete.css" location="css/jquery-ui-1.8.18.custom.css" />
        <resource type="download" name="jquery-autocomplete.js" location="js/jquery-ui-1.8.18.custom.min.js" />
        <resource type="download" name="bamboo-plan-ownership.css" location="/css/bamboo-plan-ownership.css"/>
        <resource type="download" name="bamboo-plan-ownership.js" location="/js/bamboo-plan-ownership.js"/>
        <resource type="download" name="images/" location="/images"/>

        <context>bamboo-plan-ownership</context>
    </web-resource>

    <web-resource key="bamboo-plan-ownership-warning" name="bamboo-plan-ownership Warning">
        <dependency>com.atlassian.auiplugin:jquery</dependency>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <resource type="download" name="bamboo-plan-ownership-warning.js" location="/js/bamboo-plan-ownership-warning.js"/>

        <context>atl.general</context>
    </web-resource>

    <xwork key="planOwnershipAdmin" name="Enable Plan Ownership">
        <package name="planOwnershipEnabler" extends="admin">
            <action name="viewPlanOwnershipEnabler" class="com.atlassian.buildeng.ownership.config.PlanOwnershipConfig" method="default">
                <result name="input" type="freemarker">/template/planOwnershipAdmin.ftl</result>
            </action>
            <action name="configPlanOwnershipEnabler" class="com.atlassian.buildeng.ownership.config.PlanOwnershipConfig" method="save">
                <result name="input" type="freemarker">/template/planOwnershipAdmin.ftl</result>
                <result name="success" type="freemarker">/template/planOwnershipAdmin.ftl</result>
            </action>
        </package>
    </xwork>

    <additionalBuildConfigurationPlugin key="ownership" name="Plan Ownership Configuration"
        class="com.atlassian.buildeng.ownership.plan.PlanOwnershipPlanConfigurationPlugin">
        <description>Other setting to configure the owner of a plan</description>
        <resource type="freemarker" name="edit" location="template/planConfiguration.ftl"/>
        <resource type="freemarker" name="view" location="template/planConfiguration.ftl"/>
    </additionalBuildConfigurationPlugin>

    <web-item key="configurePlanOwnership" name="Bamboo Plan Ownership Configuration" section="system.admin/plugins" weight="10">
        <description>Menu Item to configure Plan Ownership</description>
        <label key="Plan Ownership Plugin" />
        <link linkId="">/admin/viewPlanOwnershipEnabler.action</link>
    </web-item>


    <bambooEventListener key="buildQueuedEventListener" name="BuildQueued Bamboo Event Listener"
                         class="com.atlassian.buildeng.ownership.PlanDisablerQueuedEventListener">
        <description>Listens to BuildQueued event in Bamboo to disable plans with no owners.</description>
    </bambooEventListener>

    <component-import key="pluginSettingsFactory" interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" />


    <component key="PlanOwnershipConfig" class="com.atlassian.buildeng.ownership.config.PlanOwnershipConfig">
    </component>

    <rest key="planOwnership" path="/ownership" version="1.0">
        <description>Provides plan ownership services.</description>
    </rest>

    <web-panel key="ownership-summary-job" name="Ownership info for job results panel."
               location="jobresult.summary.right" class="com.atlassian.buildeng.ownership.JobSummaryPanel">
    </web-panel>
    <web-panel key="ownership-summary-plan" name="Ownership info for plan results panel."
               location="chainresult.summary.right" class="com.atlassian.buildeng.ownership.PlanSummaryPanel">
    </web-panel>

</atlassian-plugin>
