${webResourceManager.requireResourcesForContext("bamboo-plan-ownership")}

[@ui.bambooSection titleKey="planownership.bamboo.plugin.plan.config.title"]
	[@ww.textfield labelKey='planownership.bamboo.plugin.plan.config.owner'
                        name='custom.planownership.bamboo.plugin.plan.config.ownerOfBuild'
                        description=i18n.getText("planownership.bamboo.plugin.plan.config.description")
                        cssClass='long-field owner-autocomplete' /]
[/@ui.bambooSection]
