/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

AJS.$(document).ready(function() {
    (function() {
        if(BAMBOO.currentPlan && (AJS.$("body").hasClass( "dec_planConfig" ) || AJS.$("body").hasClass( "dec_plan" ))) {
            AJS.$.getJSON(AJS.contextPath() + "/rest/ownership/1.0/owner/" + BAMBOO.currentPlan.key, function( data ) {
                if(data["plan.ownership.owner"] == null) {
                        AJS.messages.warning('.bamboo-page-header-extra', {
                        title: "Plan Ownership Plugin",
                        body: "This plan does not have an active owner. " + "<span id=\"plan-ownership-disable-plan\"/> "
                        + "<span id=\"plan-ownership-is-admin\"/>"
                        + " For more information, see: "
                        + "<a href="+"http://go.atlassian.com/plan-ownership"
                        + ">go/plan-ownership</a>"
                    });
                }

                if(data["plan.ownership.stopPlans"]) {
                    AJS.$("#plan-ownership-disable-plan")
                        .replaceWith('This plan will be prevented from running until an active owner is added.');
                } else {
                    AJS.$("#plan-ownership-disable-plan")
                        .replaceWith('In the near future, this plan will be prevented from running until an active owner is added.');
                }

                if(data["plan.ownership.currentUserCanEditPlan"]) {
                    AJS.$("#plan-ownership-is-admin").replaceWith("Please add an owner in the " +
                        "<a href="+AJS.contextPath() + "/chain/admin/config/editChainMiscellaneous.action?buildKey=" + BAMBOO.currentPlan.key +
                        ">Other configuration for this plan.</a>")
                } else {
                    AJS.$("#plan-ownership-is-admin").replaceWith("Please have a plan admin add an owner to this plan. ")
                }
            }).fail(function() {});
        }
    })();
});
